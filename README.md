# Base Converter
This is a simple base converter written in python3, using pyqt5 for it's gui.
In order to run it, simply ensure that [python3](https://www.python.org/downloads/) 
is installed along with [pyqt5](https://www.riverbankcomputing.com/software/pyqt/download5) 
(It's probably easiest to install pyqt5 with pip), 
navigate to the directory containing base_converter.py and enter the command
 `python3 base_converter.py`




