"""
This is a simple base converter that converts numbers the hard way (i.e without 
python's built in base conversion). It's gui uses pyqt5, and the majority of 
the gui code was generated using Qt Designer.
"""
from PyQt5 import QtCore, QtGui, QtWidgets

def convert(number, original_base, new_base):
    # Convert all numbers to base 10, This is largely due to the fact that Python's math functions use base 10
    if original_base != 10:
        number = str(number).lower()
        result = 0
        base_count = len(number) -1
        for digit in number:
            # convert hex values higher than 9 to their decimal equivelent.
            if original_base == 16:
                if digit == 'a':
                    digit = 10
                elif digit == 'b':
                    digit = 11
                elif digit == 'c':
                    digit = 12
                elif digit == 'd':
                    digit = 13
                elif digit == 'e':
                    digit = 14
                elif digit == 'f':
                    digit = 15

            digit = int(digit)
            result += (original_base ** base_count) * digit
            base_count -= 1
        # return the result now if the new base is 10
        if new_base == 10:
            return result
        number = result
        original_base = 10

    # Start converting the number
    number = int(number)
    if number == 0:
        return 0
    base_count = 0
    result = []
    # find largest place value the original number fits into
    while True:
        place_value = new_base ** base_count

        if place_value == number:
            break

        elif place_value > number:
            base_count -= 1
            break

        base_count += 1

    remaining = number
    for exponent in range(base_count, -1, -1):
        for place in range(new_base-1, -1, -1):
            value = place * (new_base ** exponent)
            if value <= remaining:
                remaining -= value
                result.append(place)
                break

    if new_base == 16:
        count = 0
        for digit in result:
            if digit == 10:
                result[count] = 'A'
            elif digit == 11:
                result[count] = 'B'
            elif digit == 12:
                result[count] = 'C'
            elif digit == 13:
                result[count] = 'D'
            elif digit == 14:
                result[count] = 'E'
            elif digit == 15:
                result[count] = 'F'
            count += 1

    # convert the result from list form to integer form.
    result = str(''.join(map(str, result)))
    return result


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.setWindowModality(QtCore.Qt.NonModal)
        Dialog.resize(500, 375)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(Dialog.sizePolicy().hasHeightForWidth())
        Dialog.setSizePolicy(sizePolicy)
        Dialog.setMinimumSize(QtCore.QSize(423, 320))
        Dialog.setMaximumSize(QtCore.QSize(1920, 1080))
        Dialog.setContextMenuPolicy(QtCore.Qt.DefaultContextMenu)
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap("icon.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        Dialog.setWindowIcon(icon)
        Dialog.setModal(False)
        Dialog.setFixedSize(500,375)
        Dialog.setWindowFlags(Dialog.windowFlags() |
                              QtCore.Qt.Window |
                              QtCore.Qt.WindowSystemMenuHint |
                              QtCore.Qt.WindowMinimizeButtonHint)
                              #QtCore.Qt.WindowMinMaxButtonsHint)
        self.Number_Entry = QtWidgets.QLineEdit(Dialog)
        self.Number_Entry.setGeometry(QtCore.QRect(150, 60, 340, 27))
        self.Number_Entry.setObjectName("Number_Entry")
        self.Base_Label = QtWidgets.QLabel(Dialog)
        self.Base_Label.setGeometry(QtCore.QRect(10, 100, 181, 31))
        self.Base_Label.setObjectName("Base_Label")
        self.Number_Label = QtWidgets.QLabel(Dialog)
        self.Number_Label.setGeometry(QtCore.QRect(10, 60, 141, 21))
        self.Number_Label.setObjectName("Number_Label")
        self.line = QtWidgets.QFrame(Dialog)
        self.line.setGeometry(QtCore.QRect(-10, 130, 520, 20))
        self.line.setFrameShape(QtWidgets.QFrame.HLine)
        self.line.setFrameShadow(QtWidgets.QFrame.Sunken)
        self.line.setObjectName("line")
        self.Binary_Label = QtWidgets.QLabel(Dialog)
        self.Binary_Label.setGeometry(QtCore.QRect(60, 180, 61, 21))
        self.Binary_Label.setObjectName("Binary_Label")
        self.Binary_box = QtWidgets.QLineEdit(Dialog)
        self.Binary_box.setGeometry(QtCore.QRect(120, 180, 301, 27))
        self.Binary_box.setObjectName("Binary_box")

        self.copy_bin_button = QtWidgets.QPushButton(Dialog)
        self.copy_bin_button.setGeometry(QtCore.QRect(430, 180, 60, 26))
        self.copy_bin_button.setObjectName("copy_bin_button")
        
        self.copy_oct_button = QtWidgets.QPushButton(Dialog)
        self.copy_oct_button.setGeometry(QtCore.QRect(430, 220, 60, 26))
        self.copy_oct_button.setObjectName("copy_oct_button")

        self.copy_dec_button = QtWidgets.QPushButton(Dialog)
        self.copy_dec_button.setGeometry(QtCore.QRect(430, 260, 60, 26))
        self.copy_dec_button.setObjectName("copy_dec_button")
        
        self.copy_hex_button = QtWidgets.QPushButton(Dialog)
        self.copy_hex_button.setGeometry(QtCore.QRect(430, 300, 60, 26))
        self.copy_hex_button.setObjectName("copy_hex_button")
        
        self.Reset_Button = QtWidgets.QPushButton(Dialog)
        self.Reset_Button.setGeometry(QtCore.QRect(10, 340, 97, 26))
        self.Reset_Button.setObjectName("Reset_Button")
        self.Base_ComboBox = QtWidgets.QComboBox(Dialog)
        self.Base_ComboBox.setGeometry(QtCore.QRect(190, 100, 181, 31))
        self.Base_ComboBox.setObjectName("Base_ComboBox")
        self.Base_ComboBox.addItem("")
        self.Base_ComboBox.addItem("")
        self.Base_ComboBox.addItem("")
        self.Base_ComboBox.addItem("")
        self.Octal_Label = QtWidgets.QLabel(Dialog)
        self.Octal_Label.setGeometry(QtCore.QRect(70, 220, 51, 17))
        self.Octal_Label.setObjectName("Octal_Label")
        self.Octal_Box = QtWidgets.QLineEdit(Dialog)
        self.Octal_Box.setGeometry(QtCore.QRect(120, 220, 301, 27))
        self.Octal_Box.setObjectName("Octal_Box")
        self.Decimal_Label = QtWidgets.QLabel(Dialog)
        self.Decimal_Label.setGeometry(QtCore.QRect(50, 260, 71, 17))
        self.Decimal_Label.setObjectName("Decimal_Label")
        self.Decimal_Box = QtWidgets.QLineEdit(Dialog)
        self.Decimal_Box.setGeometry(QtCore.QRect(120, 260, 301, 27))
        self.Decimal_Box.setObjectName("Decimal_Box")
        self.Hex_Label = QtWidgets.QLabel(Dialog)
        self.Hex_Label.setGeometry(QtCore.QRect(20, 300, 91, 17))
        self.Hex_Label.setObjectName("Hex_Label")
        self.Hex_Box = QtWidgets.QLineEdit(Dialog)
        self.Hex_Box.setGeometry(QtCore.QRect(120, 300, 301, 27))
        self.Hex_Box.setObjectName("Hex_Box")
        self.Convert_Button = QtWidgets.QPushButton(Dialog)
        self.Convert_Button.setGeometry(QtCore.QRect(393, 340, 97, 26))
        self.Convert_Button.setObjectName("Convert_Button")
        self.Title_Label = QtWidgets.QLabel(Dialog)
        self.Title_Label.setGeometry(QtCore.QRect(200, 10, 121, 31))
        font = QtGui.QFont()
        font.setBold(True)
        font.setUnderline(True)
        font.setWeight(75)
        self.Title_Label.setFont(font)
        self.Title_Label.setObjectName("Title_Label")
        self.Results_label = QtWidgets.QLabel(Dialog)
        self.Results_label.setGeometry(QtCore.QRect(235, 150, 67, 17))
        self.Results_label.setObjectName("Results_label")

        self.retranslateUi(Dialog)
        self.Reset_Button.clicked.connect(lambda: self.reset())
        self.Convert_Button.clicked.connect(lambda: self.convert_pressed())
        self.copy_bin_button.clicked.connect(lambda: self.copy('bin'))
        self.copy_oct_button.clicked.connect(lambda: self.copy('oct'))
        self.copy_dec_button.clicked.connect(lambda: self.copy('dec'))
        self.copy_hex_button.clicked.connect(lambda: self.copy('hex'))
        self.Number_Entry.returnPressed.connect(lambda: self.convert_pressed())
        self.copy_bin_button.setDefault(False)
        self.copy_bin_button.setAutoDefault(False)
        self.copy_oct_button.setDefault(False)
        self.copy_oct_button.setAutoDefault(False)
        self.copy_dec_button.setDefault(False)
        self.copy_dec_button.setAutoDefault(False)
        self.copy_hex_button.setDefault(False)
        self.copy_hex_button.setAutoDefault(False)
        self.Reset_Button.setDefault(False)
        self.Reset_Button.setAutoDefault(False)
        self.Convert_Button.setDefault(False)
        self.Convert_Button.setAutoDefault(False)
        QtCore.QMetaObject.connectSlotsByName(Dialog)


    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Base Converter"))
        self.Base_Label.setText(_translate("Dialog", "Original base of Number:"))
        self.Number_Label.setText(_translate("Dialog", "Number to convert:"))
        self.Binary_Label.setText(_translate("Dialog", "Binary:"))
        self.Reset_Button.setText(_translate("Dialog", "Reset"))
        self.Base_ComboBox.setItemText(0, _translate("Dialog", "Binary - 2"))
        self.Base_ComboBox.setItemText(1, _translate("Dialog", "Octal - 8"))
        self.Base_ComboBox.setItemText(2, _translate("Dialog", "Decimal -10"))
        self.Base_ComboBox.setItemText(3, _translate("Dialog", "Hexidecimal - 16"))
        self.Octal_Label.setText(_translate("Dialog", "Octal:"))
        self.Decimal_Label.setText(_translate("Dialog", "Decimal:"))
        self.Hex_Label.setText(_translate("Dialog", "Hexidecimal:"))
        self.Convert_Button.setText(_translate("Dialog", "Convert"))
        self.Title_Label.setText(_translate("Dialog", "Base Converter"))
        self.Results_label.setText(_translate("Dialog", "Results:"))
        self.copy_bin_button.setText(_translate("Dialog", "Copy."))
        self.copy_oct_button.setText(_translate("Dialog", "Copy."))
        self.copy_dec_button.setText(_translate("Dialog", "Copy."))
        self.copy_hex_button.setText(_translate("Dialog", "Copy."))


    def reset(self):
        self.Number_Entry.setText('')
        self.Binary_box.setText('')
        self.Octal_Box.setText('')
        self.Decimal_Box.setText('')
        self.Hex_Box.setText('')
        self.Base_ComboBox.setCurrentIndex(0)


    def convert_pressed(self):
        hchars = '0123456789abcdef'
        bchars = '01'
        ochars = '01234567'
        dchars = '0123456789'
        bases = [2, 8, 10, 16]
        number = self.Number_Entry.text()
        original_base = int(self.Base_ComboBox.currentIndex())
        error = False

        if original_base == 0:
            original_base = 2
        elif original_base == 1:
            original_base = 8
        elif original_base ==2:
            original_base = 10
        elif original_base == 3:
            original_base = 16

        print('Converting', number + '...\noriginal base =', original_base)

        if original_base == 2:
            for n in str(number).lower():
                if n not in bchars:
                    error = True
                    break

        if original_base == 8:
            for n in str(number).lower():
                if n not in ochars:
                    error = True
                    break

        if original_base == 10:
            for n in str(number).lower():
                if n not in dchars:
                    error = True
                    break

        if original_base == 16:
            for n in str(number).lower():
                if n not in hchars:
                    error = True
                    break

        if not error:
            for new_base in bases:
                result = convert(number,original_base,new_base)
                if new_base == 2:
                    self.Binary_box.setText(str(result))
                elif new_base == 8:
                    self.Octal_Box.setText(str(result))
                elif new_base == 10:
                    self.Decimal_Box.setText(str(result))
                elif new_base == 16:
                    self.Hex_Box.setText(str(result))

        else:
            self.error('')


    def error(self,message):
            print('Error!')
            self.Binary_box.setText('**** ERROR ' + message + ' ****')
            self.Octal_Box.setText('**** ERROR ' + message + ' ****')
            self.Decimal_Box.setText('**** ERROR ' + message + ' ****')
            self.Hex_Box.setText('**** ERROR ' + message + ' ****')


    def copy(self, box):
        if box == 'bin':
            self.Binary_box.selectAll()
            self.Binary_box.copy()
            print('coppied binary box')

        elif box == 'oct':
            self.Octal_Box.selectAll()
            self.Octal_Box.copy()

        elif box == 'dec':
            self.Decimal_Box.selectAll()
            self.Decimal_Box.copy()

        elif box == 'hex':
            self.Hex_Box.selectAll()
            self.Hex_Box.copy()


if __name__ == "__main__":
    import sys
    app = QtWidgets.QApplication(sys.argv)
    Dialog = QtWidgets.QDialog()
    ui = Ui_Dialog()
    ui.setupUi(Dialog)
    Dialog.show()
    sys.exit(app.exec_())

